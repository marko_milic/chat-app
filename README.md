# Chat App

This is a simple chat app with two APIs exposed.
Also, reactjs front end included, for now it is in development.

# Setup
Change "CHAT_APP_HOST" environment variable in ".env" to match your machine IP address

# Usage
Run the app:
 ```
docker-compose up 
 ```