#!/bin/bash
#
# RUN chatapp service
#

JAR_FILE="chatapp-service.jar"
JVM_MEMORY="512m"

# RUN JAVA APP
exec java -Xmx${JVM_MEMORY} -jar ${JAR_FILE}