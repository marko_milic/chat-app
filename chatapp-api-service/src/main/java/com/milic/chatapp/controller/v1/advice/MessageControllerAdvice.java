package com.milic.chatapp.controller.v1.advice;

import com.milic.chatapp.exception.DataNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice("com.milic.chatapp.controller.v1.api")
public class MessageControllerAdvice {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageControllerAdvice.class);

    @ExceptionHandler(DataNotFoundException.class)
    @ResponseBody
    public ResponseEntity<String> handleDataNotFoundException(DataNotFoundException exception) {
        LOGGER.info("Handled DataNotFoundException exception: : {}", exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }
}
