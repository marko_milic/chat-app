package com.milic.chatapp.controller.v1.api;

public class ControllerConstants {

    public static final String API_URI = "/api";
    public static final String VERSION_1 = "/v1";

    public static final String MESSAGE_URI_PATH = "/messages";
}
