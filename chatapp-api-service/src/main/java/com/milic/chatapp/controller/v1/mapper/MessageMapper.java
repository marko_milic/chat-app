package com.milic.chatapp.controller.v1.mapper;

import com.milic.chatapp.controller.v1.model.CreateMessageRequest;
import com.milic.chatapp.model.Message;

public class MessageMapper {
    public static Message toMessage(CreateMessageRequest createMessageRequest) {
        return new Message()
                .setUser(createMessageRequest.getUser())
                .setContent(createMessageRequest.getContent());
    }
}
