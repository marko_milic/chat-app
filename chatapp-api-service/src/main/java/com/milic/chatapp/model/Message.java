package com.milic.chatapp.model;

import com.milic.chatapp.repository.AuditTrailListener;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "message")
@EntityListeners(AuditTrailListener.class)
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "user", nullable = false)
    private String user;

    @Column(name = "content", nullable = false)
    private String content;

    @Column(name = "created_date")
    @CreatedDate //TODO: investigate proper configuration for @CreatedDate annotation
    private LocalDateTime createdDate;

    public Long getId() {
        return id;
    }

    public Message setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUser() {
        return user;
    }

    public Message setUser(String user) {
        this.user = user;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Message setContent(String content) {
        this.content = content;
        return this;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public Message setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }
}
