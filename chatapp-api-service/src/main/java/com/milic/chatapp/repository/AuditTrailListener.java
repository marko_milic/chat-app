package com.milic.chatapp.repository;

import com.milic.chatapp.model.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;
import java.util.Objects;

public class AuditTrailListener {
    private static final String PREFIX = "[MESSAGE AUDIT]";
    private static Log LOGGER = LogFactory.getLog(AuditTrailListener.class);

    @PrePersist
    @PreRemove
    @PreUpdate
    private void beforeAnyUpdate(Message message) {
        if (Objects.isNull(message.getId())) {
            message.setCreatedDate(LocalDateTime.now());
            LOGGER.info(PREFIX + " About to add a message");
        } else {
            LOGGER.info(PREFIX + " About to update/delete message: " + message.getId());
        }
    }
}
