package com.milic.chatapp.service;

import com.milic.chatapp.exception.DataNotFoundException;
import com.milic.chatapp.model.Message;
import com.milic.chatapp.repository.MessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageService.class);

    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public List<Message> findSortedByCreatedDateAsc() {
        List<Message> messages = messageRepository.findAll(Sort.by(Sort.Direction.ASC, "createdDate"));

        if (messages.isEmpty()) {
            throw new DataNotFoundException("No data found.");
        }

        LOGGER.info("Message successfully retrieved");
        return messages;
    }

    public Message createMessage(Message message) {
        Message createdMessage = messageRepository.save(message);
        LOGGER.info("Message successfully created");

        return createdMessage;
    }
}
